<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct(){

        parent::__construct();

        $this->load->model("APIPerpus");
        $this->url_perpus = $this->config->item('url_perpus');
    }

        public function index(){
           

            
            $respon = [];
            $respons = json_decode($this->APIPerpus->GET($this->url_perpus."get_user"));
            if($respons != null && $respons->status == '1'){
                $respon = $respons->data;
            }

            $data['respon'] = $respon;

            // echo json_encode($respon);
            // exit;
            $this->load->view('frame/header');
            $this->load->view('page/user', $data);

        }

        function edit_users($id_user){
            $respon = [];
            $respons = json_decode($this->APIPerpus->GET($this->url_perpus."get_user_by_id/$id_user"));
             if ($respons != NULL && $respons->status == '1'){
                $respon = $respons -> data;
            }
            // echo json_encode($respon);
            // exit;

            $data['respon']=$respon;


            $this->load->view('page/edit_user',$data);
            
        }

        // function update_user
        function confirm_edit_user(){
            $id_user= $this->input->post('id_user');
            $username= $this->input->post('username');
            $role = $this->input->post('role');
            $nama = $this->input->post('nama');
            $tgl_lahir= $this->input->post('tgl_lahir');
            $no_telp = $this->input->post('no_telp');
            $alamat = $this->input->post('alamat');
            $aktif =$this->input->post('aktif');
            

            $body = array(
                "username" => $username,
                "role" => $role,
                "nama" => $nama,
                "tgl_lahir" => $tgl_lahir,
                "no_telp" => $no_telp,
                "alamat" => $alamat,
                "aktif" => $aktif
            );
           
          

     
            $respon = [];
            $respons = json_decode($this->APIPerpus->PUT($this->url_perpus."update_user/$id_user", $body));
             if ($respons != NULL && $respons->status == '1'){
                $respon = $respons -> data;
            }

      
          

            if($respon==null || $respon=="0"){
                $array=array('status' => '0','message' => 'API not response . please contact administrator');
              } else if($respon->status =='1'){
                $array=array('status' => '1','message' => 'Data has been Added.. ');
              } else {
                $array=array('status' => '0','message' => $respon->data);
              }
              redirect('user');


        }
    
}
