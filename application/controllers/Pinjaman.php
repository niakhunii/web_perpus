<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pinjaman extends CI_Controller {

    function __construct(){

        parent::__construct();

        $this->load->model("APIPerpus");
        $this->url_perpus = $this->config->item('url_perpus');
    }

        public function index(){

            $data_pinjaman = [];
            $data_pinjamans = json_decode($this->APIPerpus->GET($this->url_perpus."get_pinjam"));
            if($data_pinjamans != null && $data_pinjamans->status == '1'){
                $data_pinjaman = $data_pinjamans->data;
            }


            // echo json_encode($data_pinjaman);
            // exit;
            // $this->load->view('frame/header');


            $data['data_pinjam'] = $data_pinjaman;

            $this->load->view('frame/header');
            $this->load->view('frame/nav');
            $this->load->view('page/reportpinjaman', $data);

        }

        function add_pinjaman(){

            $data_buku = [];
            $data_bukus = json_decode($this->APIPerpus->GET($this->url_perpus."get_buku"));
            if ($data_bukus != NULL && $data_bukus->status == '1'){
                $data_buku = $data_bukus -> data;
            }


            $data_user = [];
            $data_users = json_decode($this->APIPerpus->GET($this->url_perpus."get_user"));
            if ($data_users != NULL && $data_users->status == '1'){
                $data_user = $data_users -> data;
            }

            $data['data_buku']=$data_buku;
            $data['data_user']=$data_user;

            $this->load->view('page/add_pinjaman', $data);
        }
        
        function confirmadd_pinjaman(){
            $kd_buku = $this->input->post('kd_buku');
            $tgl_pinjam = $this->input->post('tgl_pinjam');
            $id_user = $this->input->post('id_user');
            $tgl_pengembalian = $this->input->post('tgl_pengembalian');
            

            $body = array(
                "kd_buku" => $kd_buku,
                "tgl_pinjam" => $tgl_pinjam,
                "id_user_pinjam" => $id_user,
                "tgl_pengembalian" => $tgl_pengembalian
            );
            // echo json_encode($body);
            // exit;
          
            $respon = [];
            $respons = json_decode($this->APIPerpus->POST($this->url_perpus."insert_pinjam", $body));
             if ($respons != NULL && $respons->status == '1'){
                $respon = $respons -> data;
            }
            
            // echo json_encode($respons);
            // exit;
        

            if($respon==null || $respon=="0"){
                $array=array('status' => '0','message' => 'API not response . please contact administrator');
              } else if($respon->status =='1'){
                $array=array('status' => '1','message' => 'Data has been Added.. ');
              } else {
                $array=array('status' => '0','message' => $respon->data);
              }
              redirect('pinjaman');


            // $this->load->view('page/add_pinjaman');
        }

        

}
