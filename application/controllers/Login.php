<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct(){

        parent::__construct();

        $this->load->model("APIPerpus");
        $this->url_perpus = $this->config->item('url_perpus');
    }

        public function index(){

            $this->load->view('frame/header');
            $this->load->view('page/login');

        }
        function register(){

            // $this->load->view('frame/header');
            $this->load->view('page/register');

        }

        function ConfirmLogin(){
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $body = array(
                "username" => $username,
                "password" => $password
            );

            $respon = [];
            $respons = json_decode($this->APIPerpus->POST($this->url_perpus."login", $body));
             if ($respons != NULL && $respons->status == '1'){
                $data_buku = $respons -> data;
            }

            if($respon==null || $respon=="0"){
                $array=array('status' => '0','message' => 'API not response . please contact administrator');
              } else if($respon->status =='1'){
                $array=array('status' => '1','message' => 'Data has been Added.. ');
              } else {
                $array=array('status' => '0','message' => $respon->data);
              }
              redirect('buku');

      
        }

        function confirmregister(){
            $nama = $this->input->post('nama');
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $tgl_lahir = $this->input->post('tgl_lahir');
            $no_telp = $this->input->post('no_telp');
            $alamat = $this->input->post('alamat');
            $role = '1';
            $aktif = '1';
            $id_user = '';

            $body = array(
                "username" => $username,
                "password" => $password,
                "role" => $role,
                "nama" =>  $nama,
                "tgl_lahir" => $tgl_lahir, //atasan penilai
                "no_telp" => $no_telp,  //atasan pegawai kinerja
                "alamat" =>  $alamat, 
                "aktif" => $aktif

            );
        
    

            $respon = [];
            $respons = json_decode($this->APIPerpus->POST($this->url_perpus."user", $body));
             if ($respons != NULL && $respons->status == '1'){
                $data_buku = $respons -> data;
            }
            redirect("login");

            

        }
    
}
