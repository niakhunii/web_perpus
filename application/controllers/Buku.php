<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buku extends CI_Controller {

    function __construct(){

        parent::__construct();

        $this->load->model("APIPerpus");
        $this->url_perpus = $this->config->item('url_perpus');
    }

        public function index(){
            $data_buku = [];
            $data_bukus = json_decode($this->APIPerpus->GET($this->url_perpus."get_buku"));
            if ($data_bukus != NULL && $data_bukus->status == '1'){
                $data_buku = $data_bukus -> data;
            }


            $data['data_buku'] = $data_buku;

            $this->load->view('frame/header');
            $this->load->view('frame/nav');
            $this->load->view('page/bukureport', $data);

        }

        function add_buku(){


            $this->load->view('page/add_buku');
        }

        function confirmadd_buku(){
            $nama_buku = $this->input->post('nama_buku');
            $penerbit = $this->input->post('penerbit');
            $tgl_terbit = $this->input->post('tgl_terbit');
            $jmlh_buku = $this->input->post('jmlh_buku');
            

            $body = array(
                "nama_buku" => $nama_buku,
                "penerbit" => $penerbit,
                "tgl_terbit" => $tgl_terbit,
                "jmlh_buku" => $jmlh_buku
            );
          
            $respon = [];
            $respons = json_decode($this->APIPerpus->POST($this->url_perpus."insert_buku", $body));
             if ($respons != NULL && $respons->status == '1'){
                $respon = $respons -> data;
            }
        

            if($respon==null || $respon=="0"){
                $array=array('status' => '0','message' => 'API not response . please contact administrator');
              } else if($respon->status =='1'){
                $array=array('status' => '1','message' => 'Data has been Added.. ');
              } else {
                $array=array('status' => '0','message' => $respon->data);
              }
              redirect('buku');


            $this->load->view('page/add_buku');
        }

        function edit_buku($kd_buku){
            $respon = [];
            $respons = json_decode($this->APIPerpus->GET($this->url_perpus."get_buku_by_id/$kd_buku"));
             if ($respons != NULL && $respons->status == '1'){
                $respon = $respons -> data;
            }
            // echo json_encode($respon);
            // exit;

            $data['respon']=$respon;


            $this->load->view('page/edit_buku',$data);
            
        }

        function confirm_edit_buku(){
            $kd_buku = $this->input->post('kd_buku');
            $nama_buku = $this->input->post('nama_buku');
            $penerbit = $this->input->post('penerbit');
            $tgl_terbit = $this->input->post('tgl_terbit');
            $jmlh_buku = $this->input->post('jmlh_buku');
            

            $body = array(
                "nama_buku" => $nama_buku,
                "penerbit" => $penerbit,
                "tgl_terbit" => $tgl_terbit,
                "jmlh_buku" => $jmlh_buku
            );
           
          

     
            $respon = [];
            $respons = json_decode($this->APIPerpus->PUT($this->url_perpus."update_buku/$kd_buku", $body));
             if ($respons != NULL && $respons->status == '1'){
                $respon = $respons -> data;
            }

      
          

            if($respon==null || $respon=="0"){
                $array=array('status' => '0','message' => 'API not response . please contact administrator');
              } else if($respon->status =='1'){
                $array=array('status' => '1','message' => 'Data has been Added.. ');
              } else {
                $array=array('status' => '0','message' => $respon->data);
              }
              redirect('buku');


            $this->load->view('page/add_buku');
        }


        function delete_buku ($kd_buku) {
            $respon = [];
            $respons = json_decode($this->APIPerpus->Apidel($this->url_perpus."delete_buku/$kd_buku"));
             if ($respons != NULL && $respons->status == '1'){
                $respon = $respons -> data;
            }

            // echo json_encode($respons);
            // exit;

            if($respon==null || $respon=="0"){
                $array=array('status' => '0','message' => 'API not response . please contact administrator');
              } else if($respon->status =='1'){
                $array=array('status' => '1','message' => 'Data has been Added.. ');
              } else {
                $array=array('status' => '0','message' => $respon->data);
              }
            redirect('buku');
        }

    
}
