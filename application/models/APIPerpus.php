<?php  
class APIPerpus extends CI_Model{


    function __construct(){
        parent::__construct();
        $this->authorization = $this->config->item('core-authorization');
        // $this->url  = $this->config->item('url-corelogbook');
        $this->url = $this->config->item('url-nki');
        

    }

    function GET($url){
        $header = array (
            "Postman-Token: aa4e4dbf-77bb-4125-b55c-75784df6df0d",
            "User-Agent: ".$_SERVER['HTTP_USER_AGENT'],
            "cache-control: no-cache"
        );

     
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
  
       
    }


    function POST($url, $body) {
        $header = array (
            "Postman-Token: aa4e4dbf-77bb-4125-b55c-75784df6df0d",
            "User-Agent: ".$_SERVER['HTTP_USER_AGENT'],
            "Content-type: application/json"
        );


        $arr=json_encode($body);
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr);
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;

       

    }

    function PUT($url, $body) {
        $header = array (
            "Postman-Token: aa4e4dbf-77bb-4125-b55c-75784df6df0d",
            "Content-type: application/json"
        );
        $arr = json_encode($body); 
        $curl = curl_init($url); 
        curl_setopt($curl, CURLOPT_HEADER, false); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT"); 
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header); 
        curl_setopt($curl, CURLOPT_POST, true); 
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr); 
        $json_response = curl_exec($curl); 
        curl_close($curl); 
        return $json_response; 
    }

    function Apidel($url) {
        $header = array (
            "Authorization: $this->authorization",
            "content-type:application/json"
        );
        $curl=curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        $json_response=curl_exec($curl);
        curl_close($curl);
        return $json_response;
    }

   




}; 

?>